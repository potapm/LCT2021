from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import numpy as np
import h3
from functools import lru_cache
from tqdm.auto import tqdm
import json


@lru_cache(10000)
def g_h3(xii, yij):
    return h3.geo_to_h3(xii, yij,  9)


MSK = [[37.376684908203096, 55.7957880895471],
       [37.364325289062464, 55.7315234841734],
       [37.39453769140622, 55.697412346005066],
       [37.40827060156247, 55.68267328730198],
       [37.453589205078075, 55.636869056546395],
       [37.4989078085937, 55.594899347399334],
       [37.596411470703096, 55.57156334252638],
       [37.69116855078122, 55.57234143382528],
       [37.817511324218714, 55.631431083789636],
       [37.850470308593714, 55.65084893483958],
       [37.833990816406214, 55.69198281565908],
       [37.846350435546846, 55.7082691348538],
       [37.85321689062496, 55.763281945079946],
       [37.84909701757809, 55.81821691873245],
       [37.7186343710937, 55.896229854864345],
       [37.633490328124964, 55.89854466574232],
       [37.58817172460933, 55.91397319398547],
       [37.54285312109371, 55.91243061851775],
       [37.474188570312464, 55.88388185420568],
       [37.44946933203121, 55.88388185420568],
       [37.411017183593714, 55.87461826417799],
       [37.39453769140622, 55.85222208945518],
       [37.40003085546871, 55.83135882022269],
       [37.376684908203096, 55.7957880895471]]


def interpolate_uber(dt=''):
    """
    df - globally defined
    """

    ns = dt
    ns = df.set_index(df.iloc[:, 0]).loc[ns]

    for i in range(len(MSK)):
        # on edges
        ns = ns.append({'longitude': MSK[i][0],
                        'latitude': MSK[i][1],
                        'NO2': ns['NO2'].min(),
                        'CO': ns['CO'].min(),
                        'NO': ns['NO'].min(),
                        'PM10': ns['PM10'].min(),
                        'PM2.5': ns['PM2.5'].min(), },
                       ignore_index=True)

    # "NO2" "CO" "NO" "PM10" "PM2.5"
    x = list(ns['latitude'].values)
    y = list(ns['longitude'].values)
    xi = np.linspace(min(x), max(x), 200)
    yi = np.linspace(min(y), max(y), 200)

    z = list(ns['NO2'].values)
    # should be linear
    ziNO2 = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')

    z = list(ns['CO'].values)
    ziCO = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')

    z = list(ns['NO'].values)
    ziNO = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')

    z = list(ns['PM10'].values)
    ziPM10 = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')

    z = list(ns['PM2.5'].values)
    ziPM2and5 = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')

    df_h3 = pd.DataFrame(
        columns=['hex_id', 'NO2', 'CO', 'NO', 'PM2.5', 'PM10'])

    for i in tqdm(range(0, xi.shape[0])):
        for j in range(0, yi.shape[0]):
            df_h3 = df_h3.append({'hex_id': g_h3(xi[i], yi[j]),
                                  'NO2': np.abs(ziNO2[i, j]),
                                  'CO': np.abs(ziCO[i, j]),
                                  'NO': np.abs(ziNO[i, j]),
                                  'PM10': np.abs(ziPM10[i, j]),
                                  'PM2.5': np.abs(ziPM2and5[i, j]),
                                  'data_time': dt},
                                 ignore_index=True)

    df_h3 = df_h3.dropna().drop_duplicates(subset=['hex_id'])

    return df_h3
