CREATE TABLE IF NOT EXISTS h3 (
  hex_id varchar(250) NOT NULL,
  date_time datetime NOT NULL,
  CO_value varchar(250) NOT NULL,
  NO_value varchar(250) NOT NULL,
  PM10_value varchar(250) NOT NULL,
  PM25_value varchar(250) NOT NULL
  PRIMARY KEY (hex_id,date_time)
);
