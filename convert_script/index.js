const fs = require('fs');
let datas = [];
const pdkI = {
    "NO2": 0.0245,
    "CO": 0.3,
    "NO": 0.0,
    "PM2.5": 0.015,
    "PM10": 0.0,
}
const pdkIndex = {
    "ALL": 0.5,
    "CO": 5,
    "SO2": 0.5,
    "CH4": 50,
    "H2S": 0.008,
    "NH3": 0.2,
    // "NO": 0.2,
    "NO2": 0.4,
    "O3": 0.16,
    "PM10": 0.3,
    "PM2.5": 0.16,
    "C6H6": 0.3,
    "C10H8": 0.007,
    "C8H8": 0.003,
    "C7H8": 0.6,
    "C6H5OH": 0.01,
    "CH20": 0.05,
};


// [0,1,2,3,6].forEach(index => {
//     Object.keys(pdkIndex).forEach(pdk => {
//         fs.copyFile(`NO-${index}.json`, `${pdk}-${index}.json`, err => {
//             if(err) throw err; // не удалось скопировать файл
//             console.log('Файл успешно скопирован');
//          });
//     })

// })
fs.readFile("2020-10-05_h3.json", (err, data) => {
    console.log(data);
    datas = JSON.parse(data);
    console.log("Data getted");

    Object.keys(pdkI).forEach(index => {

        // const r = index === 3 ? 1 : index < 3 ? 0.5 : 1.5

        function reducerHemical(accum, t) {
            return { [t.hex_id]: t[index] || 0, ...accum }
        }

        let json = datas.reduce(reducerHemical, {})
        console.log("Data reduced");
        json = JSON.stringify(json);
        fs.writeFile(`../hack/assets/data/${index}-2.json`, json, (err) => {
            if (!err) {
                console.log('done');
            }
        });
        fs.writeFile(`../hack/assets/data/${index}-1.json`, json, (err) => {
            if (!err) {
                console.log('done');
            }
        });
        fs.writeFile(`../hack/assets/data/${index}-0.json`, json, (err) => {
            if (!err) {
                console.log('done');
            }
        });
    })
})
